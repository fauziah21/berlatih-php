<?php
function XO($str) {
$tamp= str.toLowerCase();
return $tamp.filter(x=> x==='x').length == $tamp.filter(x=> x==='o').length;
}

// Test Cases
echo xo('xoxoxo'); // "Benar"
echo xo('oxooxo'); // "Salah"
echo xo('oxo'); // "Salah"
echo xo('xxxooo'); // "Benar"
echo xo('xoxooxxo'); // "Benar"

?>
