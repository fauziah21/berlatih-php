<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Ubah Huruf</title>
  </head>
  <body>
    <?php
    function ubah_huruf($string){
    //kode di sini
    $abjad = "abcdefghijklmnopqrstuvwxyz";
    $output = "";
    for ($i=0; $i < strlen($string) ; $i++) {
      $posisi = strpos($abjad, $string[$i]);
      $output .= substr($abjad, $posisi + 1, 1);
    }
    return $output ."<br>";
    }

    // TEST CASES
    echo ubah_huruf('wow'); // xpx
    echo ubah_huruf('developer'); // efwfmpqfs
    echo ubah_huruf('laravel'); // mbsbwfm
    echo ubah_huruf('keren'); // lfsfo
    echo ubah_huruf('semangat'); // tfnbohbu

    ?>
  </body>
</html>
