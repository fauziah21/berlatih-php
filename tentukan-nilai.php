<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>nilai</title>
  </head>
  <body>
    <?php
    function tentukan_nilai($number)
    {
        //  kode disini
        switch ($number) {
          case ($number >= 85 && $number <= 100):
            echo "Sangat baik";
            break;
          case ($number >= 70 && $number < 85):
            echo "Baik";
            break;
          case ($number >= 60 && $number < 70):
            echo "Cukup";
            break;
          default:
            echo "Kurang";

        }
    }

    function jarak()
    {
      // code...
      echo "<br>";
    }

    //TEST CASES
    echo tentukan_nilai(98); //Sangat Baik
    jarak();
    echo tentukan_nilai(76); //Baik
    jarak();
    echo tentukan_nilai(67); //Cukup
    jarak();
    echo tentukan_nilai(43); //Kurang
    ?>
  </body>
</html>
